//
//  DetailTopicViewController.swift
//  EntregaUIAvanzada
//
//  Created by Rita Casiello on 27/10/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

class DetailTopicViewController: UIViewController {
    
//MARK: -outlets
    @IBOutlet weak var tableView: UITableView!
 
//MARK: -properties
    private var fotos = ["foto1.jpeg", "foto2.jpeg", "foto3.jpg","foto4.jpeg"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        setUpTableView()
        
        //configuración de Header del TableView
        let headerView = CustomHeader(frame: .zero)
        headerView.configure(text: "Suggested topics")
        tableView.tableHeaderView = headerView
        tableView.tableHeaderView?.backgroundColor = .white
        
    }
    private func registerNib() {
        let nib = UINib(nibName: "TopicCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "TopicCell")
    }
    
    private func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = TopicCell.estimatedRowHeight()
    }
    
     override func viewWillLayoutSubviews() {
             super.viewWillLayoutSubviews()
             updateHeaderViewHeight(for: tableView.tableHeaderView)
         }

         func updateHeaderViewHeight(for header: UIView?) {
             guard let header = header else { return }
             header.frame.size.height = header.systemLayoutSizeFitting(CGSize(width: view.bounds.width - 32.0, height: 0)).height
         }
}

extension DetailTopicViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopicCell", for: indexPath) as! TopicCell
        
        if indexPath.row == 1 {
            cell.lbl.text = "Prueba de topic.Importante que se vea"
            cell.photo.image = UIImage (named: fotos [0] )
        }
        return cell
    }
}
