//
//  TopicsViewController.swift
//  EntregaUIAvanzada
//
//  Created by Rita Casiello on 27/10/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

class TopicsViewController: UIViewController {

    //MARK: -outlets
    @IBOutlet weak var pin: UIImageView!
    @IBOutlet weak var blackView: UIView!
    
    //MARK: -properties
     private var tableView: UITableView!
     private var fotos = ["foto1.jpeg", "foto2.jpeg", "foto3.jpg","foto4.jpeg"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Temas"
        //configuración de TableView y cell customizada
        makeTable()
        registerNib()
        setUpTableView()
        
        //rotación de iconos
        pin.transform = CGAffineTransform(rotationAngle: 45.0)
    }
    
    private func makeTable() {
    
        tableView = UITableView(frame:.zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.contentInset = UIEdgeInsets(top: 300.0, left: 0, bottom: 0, right: 0)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: blackView.bottomAnchor, constant: 0.0),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0)
        ])
    }
    
    private func registerNib() {
        let nib = UINib(nibName: "TopicCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "TopicCell")
    }
    
    private func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = TopicCell.estimatedRowHeight()
    }
   
}

extension TopicsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopicCell", for: indexPath) as! TopicCell
        
        if indexPath.row == 1 {
            cell.lbl.text = "Prueba de topic.Importante que se vea"
            cell.photo.image = UIImage (named: fotos [0] )
        }
          // var i = 0
          // while i <= tableView.numberOfRows(inSection: 0){
           //   cell.photo.image = UIImage(named: fotos[2])
           // }
            
           // i+=1
    
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let detailTopicViewController = DetailTopicViewController()
        
        // push a Topic Detail
        navigationController?.pushViewController(detailTopicViewController, animated: true)
        
    }
}

