//
//  LaunchViewController.swift
//  EntregaUIAvanzada
//
//  Created by Rita Casiello on 27/10/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit


class LaunchViewController: UIViewController {


    @IBOutlet weak var launchView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        perform(Selector(("homeMadeLaunchScreen")), with: nil, afterDelay: 3.0)
        
        let newBarButton = UIBarButtonItem (title: "Ir",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(homeMadeLaunchScreen))
        
        navigationItem.setRightBarButton(newBarButton, animated: true)
        
    }
    
  @objc
    func homeMadeLaunchScreen(){
        let newViewController = TopicsViewController()
        self.navigationController?.pushViewController(newViewController, animated: true)
    
            }
}

