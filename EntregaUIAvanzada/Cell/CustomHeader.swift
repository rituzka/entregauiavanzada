//
//  CustomHeader.swift
//  EntregaUIAvanzada
//
//  Created by Rita Casiello on 03/11/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

class CustomHeader: UIView {
    
    let label = UILabel(frame: .zero)

    override init(frame: CGRect) {
        
        super.init(frame: frame)
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0),
            label.topAnchor.constraint(equalTo: topAnchor),
            label.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
    }

    func configure(text: String) {
        label.text = text
        label.font = UIFont.boldSystemFont(ofSize: 24)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
