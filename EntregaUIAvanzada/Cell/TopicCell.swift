//
//  TopicCell.swift
//  EntregaUIAvanzada
//
//  Created by Rita Casiello on 27/10/2019.
//  Copyright © 2019 Rita Casiello. All rights reserved.
//

import UIKit

class TopicCell: UITableViewCell {

 
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var foto: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        photo.layer.cornerRadius = 32
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
    static func estimatedRowHeight() -> CGFloat {
    return 88.0
        
    }
}
